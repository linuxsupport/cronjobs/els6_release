# els6_release

# The ELS product for RHEL6 reached end-of-life on 30.06.2024 and as such, this script has since been deprecated.

`els6_release` is a script that runs via nomad to rebuild ELS (Extended Lifecycle Support) packages from RedHat ELS src rpms.


`els6_release` will:
* compare the package list from the main distribution and the ELS content
* confirm if there are any packages already present in the els6-stable tag, and skip building these if they already exist
* build packages via koji (depending on the rpm, either el6-base or el6_10 build tags will be used)
* tag built packages to the els6-stable tag
* send an informational email detailing the build success/failures in the event of new ELS packages

This sript can very likely be adapted to support el7, once el7 goes into ELS.
